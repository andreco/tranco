<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Estudo HTML5 E CSS(3)</title>
	<link rel="stylesheet" type="text/css" href="css/grid.css">
  	<link rel="stylesheet" type="text/css" href="css/reset.css">
  	<link rel="stylesheet" type="text/css" href="css/style.css">
  	<link rel="stylesheet"  type="text/css" href="font-awesome/css/font-awesome.css">
	<script src="js/apps.js"></script>
	<script src="http://code.jquery.com/jquery-2.0.0.js"></script>
	<script src="http://malsup.github.io/jquery.cycle.all.js"></script>
 		 </head>
<body id="home">
	<header>
		<div class="container-12">
			<h1 class="main-title"><a href="#">Logomarca</a></h1>
			<div class="info">
				<address id="info-inform"><em>Give us a call:</em> +1 800 603 6035</address>
			</div>
			<nav>
				<ul class="main-menu grid-12">
					<li class="item-menu home"><a href="#"></a></li>
					<li class="item-menu about"><a href="#">About us</a></li>
					<li class="item-menu services"><a href="#">Services</a></li>
					<li class="item-menu price"><a href="#">Price list</a></li>
					<li class="item-menu blog"><a href="#">Blog</a></li>
					<li class="item-menu contact"><a href="#">Contact</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<section class="banner container-12">
		<div class="sliders grid-12"> 
			<img src="img/slide-1.jpg" alt="">
			<img src="img/slide-2.jpg" alt="">
			<img src="img/slide-3.jpg" alt="">
		</div>
		<div class="links-slide">
				<a href="#" id="link-proximo"><i class="fa fa-arrow-circle-o-right icon-next"></i></a>
				<a href="#" id="link-anterior"><i class="fa fa-arrow-circle-o-left icon-prev"></i></a>
		</div>
	</section>



	<section class="content-conteiner container-12">
		<div class="content"></div>
		<article class="article-bg-content grid-12">
			<p class="paragraph-bg-content">	WE ALWAYS USE ONLY RELIABLE AND PROVEN VEHICLE,
				AND OUR DRIVERS HAVE THE HIGHEST QUALIFICATIONS.
			</p>
		</article>
		<div class="articles-box">
			<article class="article-bg-box grid-4">
				<a class="link_1-box" href="#">Safety</a>
				<img src="img/page-1_img-1.jpg" alt="">
				<h2>LIQUAM RHONCUS LIBER NON.</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisc ng elit. Proin ultricies vestibulum velit, bibendum condimentum metus faucibus sed.
				</p>
			</article>
			<article class="article-bg-box grid-4">
				<a class="link_2-box" href="#">BEST IN CLASS</a>
				<img src="img/page-1_img-1.jpg" alt="">
				<h2>LIQUAM RHONCUS LIBER NON.</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisc ng elit. Proin ultricies vestibulum velit, bibendum condimentum metus faucibus sed.
				</p>
			</article>
			<article class="article-bg-box grid-4">
				<a href="#">Guiding principles</a>
				<img src="img/page-1_img-1.jpg" alt="">
				<h2>LIQUAM RHONCUS LIBER NON.</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisc ng elit. Proin ultricies vestibulum velit, bibendum condimentum metus faucibus sed.
				</p>
			</article>
		</div>

		<div class="content-end grid-12">
			<div class="content-bg-end">
				<article class="article-bg-end grid-4">
					<img src="img/page-1_img-4.png" alt="">
					<h2>TRUCKING</h2>
					<h3>SHIPPING SERVICES</h3>
					<p>
						Mes cuml dia sed in lacus ut eniasc etor ingerto aliiqt es sitet amet eism ictor ut ligulate ameti dapibus ticdu nt mtsent lusto dolor ltisim.
					</p>
					<a href="#">Read more</a>
				</article>
				<article class="article-bg-end grid-4">
					<img src="img/page-1_img-5.png" alt="">
					<h2>Logistics client</h2>
					<h3>Testimonials</h3>
					<p>
						Mes cuml dia sed in lacus ut eniasc etor ingerto aliiqt es sitet amet eism ictor ut ligulate ameti dapibus ticdu nt mtsent lusto dolor ltisim.
					</p>
					<a href="#">Read more</a>
				</article>
				<article class="article-bg-end grid-4">
					<img src="img/page-1_img-6.png" alt="">
					<h2>Specialty freight</h2>
					<h3>&#38; shipping services</h3>
					<p>
						Mes cuml dia sed in lacus ut eniasc etor ingerto aliiqt es sitet amet eism ictor ut ligulate ameti dapibus ticdu nt mtsent lusto dolor ltisim.
					</p>
					<a href="#">Read more</a>
				</article>
			</div>
		</div>
	</section>
	<footer>
		<section class="content-footer container-12">
			<div class="content-bg-footer">
				<div class="commitment">
					<h2>Our Commitment</h2>
					<ul class="menu-commitment">
						<li class="item-commitment"><a href="#">Safety</a></li>
						<li class="item-commitment"><a href="#">Service</a></li>
						<li class="item-commitment"><a href="#">Bedt in class</a></li>
						<li class="item-commitment"><a href="#">Guiding Principles</a></li>
					</ul>
				</div>
				<div class="services">
					<ul class="menu-services">
						<h3>COMPREHENSIVE SERVICES</h3>
						<li class="item-services"><a href="#">Dedicated</a></li>
						<li class="item-services"><a href="#">Temperature Controlled</a></li>
						<li class="item-services"><a href="#">Intermodal</a></li>
						<li class="item-services"><a href="#">Solutiond</a></li>
					</ul>
				</div>
				<div class="success">
					<ul class="menu-success">
						<h4>Keys to Success</h4>
						<li class="item-success"><a href="">Services</a></li>
						<li class="item-success"><a href="">Values</a></li>
						<li class="item-success"><a href="">History</a></li>
						<li class="item-success"><a href="">Our people</a></li>
					</ul>
				</div>
			</div>
			<div class="tranco-greetings">
				<p><em>TRANCO</em> © 2013 | <a href="#">PRIVACY POLICY</a></p>
				<div class="social-network">
					<a class="bg_facebook" href="#">Facebook</a>
					<a class="bg_blogs" href="#">blogger</a>
					<a class="bg_twitter" href="#">twitter</a>
					<a class="bg_google" href="#">Google</a>
				</div>
			</div>
		</section>
	</footer>
		<script type="text/javascript">

			$(document).ready(function(){
   			 $('.sliders').cycle({
   			 	fx: 'scrollHorz',
   			 	speed: '1000',
    			timeout: '3000',
    			prev: '#link-anterior',
    			next: '#link-proximo'

   			 });
			});
	</script>
</body>
</html>