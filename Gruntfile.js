module.exports = function(grunt) {

  grunt.initConfig({
   watch: {
      all: {
        files: ['**/*.js', '**/*.html', '**/*.php'],
        options: {
          spawn: false,
          livereload: true
        },
      },
      css: {
        files: '**/*.css',
        options: {
          livereload: true,
        },
      },
     },

    concat_css: {
      options: {},
      css: {
        src: [
          'public/bower/jquery-pageslide/jquery.pageslide.css',
          'public/bower/colorbox/example3/colorbox.css',
          'assets/css/normalize.css',
          'assets/css/reset.css',
          'assets/css/grid.css',
          'assets/css/partials/topbar.css',
          'assets/css/partials/content.css',
          'assets/css/partials/contact.css',
          'assets/css/partials/menu-mobile.css',
          'assets/css/partials/conf.css',
          'assets/css/partials/modals.css',
          'assets/css/partials/footer.css',
          'assets/css/partials/midia.css',
          'assets/css/partials/church.css',
          'assets/css/partials/mediaqueries.css'
        ],
        dest: 'assets/css/style.min.css'
      }
    },

    jshint: {
      all: ['assets/**/*.js', 'Gruntfile.js']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-concat-css');

};
